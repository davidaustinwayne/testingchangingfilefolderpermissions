This is a Python script that uses the modules : sys, os, subprocess
to change the permissions of files and folders for Windows

The Python script was built and tested running on Python version:
Python 3.8.3 (tags/v3.8.3:6f8c832, May 13 2020, 22:20:19) [MSC v.1925 32 bit (Intel)] on win32

The function getstatusoutput part of the module subprocess is used to call the DOS/WINDOWS command icacls that makes changes to the permissions of the files and folders.

I have used two methods to detect if an error has occurred when trying to change the permissions on a folder.
2) used a try catch exception and recorded the error to e and wrote out the error

For the DOS/Windows command to fully work for (OI) and (CI) switches, the command has to be run in cmd
If the command is run in Bash or Powershell the (OI) and (CI) fail and the permissions are not set

Set the default shell to "cmd" on Visual Studio Code if the code will be run from it.

For the code to work create two folders on the C: drive called and put some files in the folders for testing
C:\PermissionsTest
C:\PermissionsTest\TestFull

The new folder names can be change in the code if needed on lines 14 and 15 of the Python script file:
setpermission.py

Python code that sets the folders is
applicationFolder = "\PermissionsTest"
configAppFolder = "\TestFull"

