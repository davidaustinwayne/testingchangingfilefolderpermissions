# import the required modules and specific functions from a module if needed
import sys, os, subprocess
from subprocess import getstatusoutput

# clear the screen and add introduction
os.system('cls')
print('Hello World')
print('Setting some file and folder permissions using the module sys')
print('The operating system platform being tested here is:')

# Setting variables for folder paths that are going to be used for testing
# With the code below the basePath folder is changed to os.chdir(applicationFolder)
basePath = '.'
applicationFolder = "\PermissionsTest"
configAppFolder = "\TestFull"
group = "Everyone"
test_group = "Authenticated Users"

# Build icacls strings to set permissions on the folders for the Everyone group
set_readaccess_appfolder = f"icacls {basePath} /grant {group}:(OI)(CI)RX /T /C"
set_fullaccess_confolder = f"icacls {basePath}{configAppFolder} /grant {group}:(OI)(CI)F /T /C"

# Please note if there are spaces in the group name extra quotes are required
set_readaccess_appfolderTest = f'icacls {basePath} /grant "{test_group}":(OI)(CI)RX /T /C'

check_permissions_appfolder = f"icacls {basePath}"
check_permissions_confolder = f"icacls {basePath}{configAppFolder}"

# Output some information and change folder location to the app folder
print(sys.platform)
print(os.DirEntry)
print(subprocess.getstatusoutput('dir'))

# Change folder path to .
os.chdir(basePath)

# Change folder path to .\PermissionsTest so basefolder = app folder
os.chdir(applicationFolder)

input("Press Enter to continue...")
print(subprocess.getstatusoutput('dir'))

# Make permission changes to the app and config folder using different error trapping methods
input("Make change to the permission on the file...")
# Using tResult to catch an error and repport
print("------------------------------------------------------------")
print("Using tResult = 0 to find error")
tResult = subprocess.getstatusoutput(set_readaccess_appfolder)
if tResult[0] != 0:
    print("There were problems setting the file permissions")

# Using try catch to report if there is an error
print("------------------------------------------------------------")
print("Using try catch to report any errors")
try:
   subprocess.getstatusoutput(set_readaccess_appfolder)
except subprocess.CalledProcessError as e:
    print("There was an error setting the permissions")
    print(e) 

# Print out all the text when changing the permissions 
print("------------------------------------------------------------")
print("Print out all results when trying to make the permission change")
print(subprocess.getstatusoutput(set_readaccess_appfolder))
print(subprocess.getstatusoutput(set_readaccess_appfolderTest))
print(subprocess.getstatusoutput(set_fullaccess_confolder))

# Now show the change permissions on the folders and files
print("------------------------------------------------------------")
print("The permissions on the folder for the application has been changed too")
print(subprocess.getstatusoutput(check_permissions_appfolder))
print("------------------------------------------------------------")
print("The permissions on the folder for the config folder has been changed too")
print(subprocess.getstatusoutput(check_permissions_confolder))


